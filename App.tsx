import "unstyled-web-components";
import "unstyled-web-components/dist/index.css";
import { createRoot } from "react-dom/client";
import { createElement, useLayoutEffect, useRef } from "react";

function wrapNative<Tagname extends keyof HTMLElementTagNameMap>(
  tagname: Tagname,
) {
  type EventHandlers = {
    [k in keyof HTMLElementEventMap as `on${Capitalize<k>}`]?:
      | ((event: HTMLElementEventMap[k]) => void)
      | undefined;
  };

  return ({
    children,
    ...props
  }: React.PropsWithChildren<
    Partial<Omit<HTMLElementTagNameMap[Tagname], "children">> & EventHandlers
  >) => {
    const ref = useRef<HTMLElement>(null);

    const proppedEvents = Object.keys(props).filter((key) =>
      /on[A-Z]/.test(key),
    );

    useLayoutEffect(function subscribeToProppedEvents() {
      const element = ref.current;
      if (element == null) {
        return;
      }
      const unsubscribes = [] as Array<() => void>;
      proppedEvents
        .map((key) => ({
          eventname: key.replace(/on(.*)$/, (_, eventname) =>
            eventname.toLowerCase(),
          ),
          handler: props[key as keyof typeof props] as Parameters<
            (typeof element)["addEventListener"]
          >[1],
        }))
        .forEach(({ eventname, handler }) => {
          element.addEventListener(eventname, handler);
          unsubscribes.push(() =>
            element.removeEventListener(eventname, handler),
          );
        });

      return () => unsubscribes.forEach((unsub) => unsub());
    }, proppedEvents);

    return createElement(tagname, { ref, ...props }, children);
  };
}

const Input = wrapNative("uwc-input");
const Form = wrapNative("form");

function App() {
  return (
    <Form onChange={console.log}>
      <Input onChange={console.log} />
    </Form>
  );
}

const container = document.querySelector("#root")!;

const root = createRoot(container);

root.render(<App />);
