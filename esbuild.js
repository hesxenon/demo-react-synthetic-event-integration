import * as Esbuild from "esbuild";

const ctx = await Esbuild.context({
  entryPoints: ["./App.tsx"],
  bundle: true,
  outdir: "./bundle",
  jsx: "transform",
  format: "esm",
});

const server = await ctx.serve({
  servedir: ".",
  fallback: "index.html",
});

console.log(`server started on ${server.host}:${server.port}`);
